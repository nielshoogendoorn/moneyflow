//
//  AppDelegate+Dependencies.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

extension AppDelegate {
    func registerDependencies() {
        container.register(TransactionServiceDelegate.self) { _ in
            TransactionService()
        }.inObjectScope(.container)
    }
}

//
//  AmountView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class AmountView: UIView {
    let euroLabel = StandardLabel()
    let centsLabel = StandardLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubviews(views: euroLabel, centsLabel)
        euroLabel.snp.makeConstraints {
            $0.leading.top.height.equalToSuperview()
            $0.bottom.lessThanOrEqualToSuperview()
        }
        
        centsLabel.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.top.trailing.equalToSuperview()
            $0.leading.equalTo(self.euroLabel.snp.trailing)
        }
        
        // Adjust the contenthugging so that the frame of the amountview will
        // always be hugging its content. The eurolabel priority should be higher
        // than the cents priority otherwise it can create spacing between the
        // two views.
        euroLabel.setContentHuggingPriority(.init(252), for: .horizontal)
        centsLabel.setContentHuggingPriority(.init(251), for: .horizontal)
        
        // Set standard styling
        setFont()
        setTextColor()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setAmount(_ amount: Double) {
        let amountString = amount.formatAsCurrencyAmount()
        let euroAmount = amountString.dropLast(2)
        let centsAmount = amountString.suffix(2)
        euroLabel.text = String(euroAmount)
        centsLabel.text = String(centsAmount)
    }
    
    func setFont(size: CGFloat = 16, weight: UIFont.Weight = .regular) {
        euroLabel.font = UIFont.systemFont(ofSize: size, weight: weight)
        centsLabel.font = UIFont.systemFont(ofSize: size * 0.6, weight: weight)
    }
    
    func setTextColor(_ color: UIColor? = UIColor.textColor) {
        euroLabel.textColor = color
        centsLabel.textColor = color
    }
}

//
//  EmptyListView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class EmptyListView: UIView {
    let emptyListImageView = UIImageView(image: #imageLiteral(resourceName: "ic_empty_list"))
    let emptyListLabel = StandardLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubviews(views: emptyListImageView, emptyListLabel)
        emptyListImageView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.centerX.centerY.equalToSuperview()
            $0.width.equalToSuperview().multipliedBy(0.4)
            $0.height.equalTo(self.emptyListImageView.snp.width)
        }
        emptyListImageView.contentMode = .scaleAspectFit
        
        emptyListLabel.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.top.equalTo(self.emptyListImageView.snp.bottom).offset(8)
            $0.centerX.equalToSuperview()
        }
        emptyListLabel.text = .couldNotFindTransactions
        emptyListLabel.textAlignment = .center
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

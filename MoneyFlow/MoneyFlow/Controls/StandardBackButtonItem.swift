//
//  StandardBackButtonItem.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class StandardBackButton: UIBarButtonItem {
    convenience init(tintColor: UIColor) {
        self.init(title: .empty,
                   style: .plain,
                   target: nil,
                   action: nil)
        self.tintColor = tintColor
    }
}

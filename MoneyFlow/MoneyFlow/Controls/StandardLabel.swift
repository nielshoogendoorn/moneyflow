//
//  StandardLabel.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class StandardLabel: UILabel {
    init(fontsize: CGFloat = 16, numberOfLines: Int = 1) {
        super.init(frame: .zero)
        self.textColor = .textColor
        self.font = UIFont.systemFont(ofSize: fontsize)
        self.numberOfLines = numberOfLines
        self.lineBreakMode = numberOfLines == 0 ? .byWordWrapping : .byTruncatingTail
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class StandardBoldLabel: StandardLabel {
    override init(fontsize: CGFloat = 16, numberOfLines: Int = 1) {
        super.init(fontsize: fontsize, numberOfLines: numberOfLines)
        self.font = UIFont.boldSystemFont(ofSize: fontsize)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  Date.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

extension Date {
    func getTimeOfDayString() -> String {
        let calendar = Calendar.autoupdatingCurrent
        let components = calendar.dateComponents([.hour, .minute], from: self)
        let timeDate = calendar.date(from: components)
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm"
        guard let time = timeDate else { return .empty }
        return formatter.string(from: time)
    }
    
    func getDayInYear() -> Date {
        let calendar = Calendar.autoupdatingCurrent
        let components = calendar.dateComponents([.day, .month, .year], from: self)
        let dayInYear = calendar.date(from: components)
        return dayInYear ?? Date()
    }
    
    func getDayInYearString(dateStyle: DateFormatter.Style) -> String {
        let formatter = DateFormatter()
        formatter.dateStyle = dateStyle
        return formatter.string(from: self)
    }
}

//
//  Double.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

extension Double {
    func formatAsCurrencyAmount() -> String {
        let formatter = NumberFormatter()
        // Normally I would use the current locale, but for this example I want
        // to force the Euro.
        formatter.locale = Locale.init(identifier: "nl_NL")
        formatter.numberStyle = .currency
        let amountString = formatter.string(from: self as NSNumber)
        return amountString ?? .empty
    }
    
    func isNegative() -> Bool {
        return self < 0.00
    }
}

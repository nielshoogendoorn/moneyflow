//
//  String+Localization.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

extension String {
    // MARK: - General
    static let moneyFlow = NSLocalizedString("MoneyFlow", comment: "")
    static let description = NSLocalizedString("Description", comment: "")
    static let date = NSLocalizedString("Date", comment: "")
    
    // MARK: - Transaction
    static let transaction = NSLocalizedString("Transaction", comment: "")
    static let balanceBefore = NSLocalizedString("Balance before", comment: "")
    static let balanceAfter = NSLocalizedString("Balance after", comment: "")
    
    // MARK: - Account
    static let account = NSLocalizedString("Account", comment: "")
    static let checkingsAccount = NSLocalizedString("checkings account", comment: "")
    static let currentBalance = NSLocalizedString("Current balance", comment: "")
    
    // MARK: - Errors
    static let couldNotFindTransactions = NSLocalizedString("Could not find transactions", comment: "")
}

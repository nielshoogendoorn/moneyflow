//
//  String+Extension.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

extension String {
    static var empty = ""
    
    func formatToDate() -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return formatter.date(from: self)
    }
    
}

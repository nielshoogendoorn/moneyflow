//
//  UIColor.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

extension UIColor {
    static var brandPrimary: UIColor {
        return UIColor(red: 0.47,
                       green: 0.66,
                       blue: 0.66,
                       alpha: 1.0)
    }
    
    static var textColor: UIColor {
        return UIColor(red: 0.0,
                       green: 0.0,
                       blue: 0.0,
                       alpha: 0.7)
    }
    
    static var incomingAmountColor: UIColor {
        return UIColor(red: 0.40,
                       green: 0.73,
                       blue: 0.42,
                       alpha: 1.0)
   
    }
    
    static var grayBackgroundColor: UIColor {
        return UIColor(red: 0.95,
                       green: 0.95,
                       blue: 0.95,
                       alpha: 1.0)
    }
    
    static var outgoingAmountColor: UIColor {
        return UIColor(red: 0.94,
                       green: 0.33,
                       blue: 0.31,
                       alpha: 1.0)
    }
}

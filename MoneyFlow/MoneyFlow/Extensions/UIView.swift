//
//  UIView+Extension.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

extension UIView {
    func addSubviews(views: UIView...) {
        views.forEach { self.addSubview($0) }
    }
    
    func makeCardView() {
        self.applyShadow(shadowColor: .black,
                         shadowOffset: CGSize(width: 0,
                                              height: 3),
                         shadowOpacity: 0.1,
                         shadowRadius: 8,
                         cornerRadius: 8)
    }
    
    func applyShadow(
        shadowColor: UIColor?,
        shadowOffset: CGSize,
        shadowOpacity: Float,
        shadowRadius: CGFloat,
        cornerRadius: CGFloat) {
        
        self.layer.masksToBounds = false
        
        if let shadowColor = shadowColor {
            self.layer.shadowColor = shadowColor.cgColor
        } else {
            self.layer.shadowColor = UIColor.black.cgColor
        }
        
        self.layer.shadowOpacity = shadowOpacity
        self.layer.shadowOffset = shadowOffset
        self.layer.shadowRadius = shadowRadius
        self.layer.cornerRadius = cornerRadius
    }
}

//
//  UIViewController.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit
import Swinject

extension UIViewController {
    var container: Container? {
        return (UIApplication.shared.delegate as? AppDelegate)?.container
    }
    
    var coordinator: Coordinator? {
        return self.navigationController as? Coordinator
    }
}

//
//  AccountViewController.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit
import Swinject
import SnapKit

protocol AccountViewControllerDelegate: class {
    func handleTransactionTap(transaction: Transaction)
}

class AccountViewController: UIViewController, AccountViewControllerDelegate {
    var transactionService: TransactionServiceDelegate?
    var transactionTableView: TransactionTableView!
    var headerView: AccountHeaderView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDependencies()
        buildInterface()
        transactionService?.loadAccountData()
        self.title = .moneyFlow        
        self.navigationItem.backBarButtonItem = StandardBackButton(tintColor: .white)
    }
    
    fileprivate func buildInterface() {
        view.addSubviews(views: transactionTableView, headerView)
        
        headerView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalTo(self.view).multipliedBy(0.30)
        }
        
        transactionTableView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.top.equalTo(self.headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    
    fileprivate func setDependencies() {
        let service = container?.resolve(TransactionServiceDelegate.self)
        transactionService = service
        transactionTableView = TransactionTableView(transactionService: service,
                                                    accountDelegate: self)
        headerView = AccountHeaderView(transactionService: service)
    }
    
    func handleTransactionTap(transaction: Transaction) {
        let detailViewController = TransactionDetailViewController(transaction: transaction)
        coordinator?.openViewController(viewController: detailViewController)
    }
}

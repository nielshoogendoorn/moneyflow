//
//  AccountHeaderView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class AccountHeaderView: UIView {
    
    let contentContainer = UIView()
    let balanceTextLabel = StandardLabel()
    let balanceAmountLabel = AmountView()
    
    var transactionService: TransactionServiceDelegate?
    
    let disposeBag = DisposeBag()
    
    init(transactionService: TransactionServiceDelegate?) {
        super.init(frame: .zero)
        self.transactionService = transactionService
        backgroundColor = .brandPrimary
        buildInterface()
        addObservables()
    }
    
    fileprivate func buildInterface() {
        addSubview(contentContainer)
        contentContainer.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.centerX.equalToSuperview()
            // Because the superview shows behind the navbar, use the offset from
            // the safeareainsets to let content appear in the center.
            $0.centerY.equalToSuperview().offset(self.safeAreaInsets.top)
        }
        
        contentContainer.addSubviews(views: balanceTextLabel, balanceAmountLabel)
        balanceTextLabel.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
        }
        balanceTextLabel.text = .currentBalance
        balanceTextLabel.font = UIFont.systemFont(ofSize: 16, weight: .light)
        balanceTextLabel.textAlignment = .center
        balanceTextLabel.textColor = .white
        
        balanceAmountLabel.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.bottom.centerX.equalToSuperview()
            $0.top.equalTo(self.balanceTextLabel.snp.bottom)
        }
        balanceAmountLabel.setFont(size: 36, weight: .light)
        balanceAmountLabel.setTextColor(.white)
    }
    
    func addObservables() {
        transactionService?
            .accountStore
            .currentBalance
            .asObservable()
            .subscribe({ event in
                self.setCurrentBalance(amount: event.element)
            }).disposed(by: disposeBag)
    }
    
    private func updateVerticalPositionContentContainer() {
        contentContainer.snp.updateConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.centerX.equalToSuperview()
            $0.centerY.equalToSuperview().offset(self.safeAreaInsets.top / 2)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateVerticalPositionContentContainer()
    }
    
    func setCurrentBalance(amount: Double?) {
        guard let amount = amount else { return }
        balanceAmountLabel.setAmount(amount)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

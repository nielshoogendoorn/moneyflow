//
//  CategoryIconView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class CategoryIconView: UIView {
    
    let iconImageView = UIImageView()
    let size: CGFloat = 16
    let iconSpacing: CGFloat = 12
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        addSubview(iconImageView)
        iconImageView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.equalToSuperview().offset(self.iconSpacing)
            $0.trailing.bottom.equalToSuperview().offset(-self.iconSpacing)
            $0.height.width.equalTo(self.size)
        }
        
        iconImageView.contentMode = .scaleAspectFit
    }
    
    func setCategoryStyling(category: TransactionCategory) {
        switch category {
        case .coffee:
            iconImageView.image = UIImage(named: "ic_category_coffee")
        case .defaultTransaction:
            iconImageView.image = UIImage(named: "ic_category_default_transaction")
        case .dinner:
            iconImageView.image = UIImage(named: "ic_category_dinner")
        case .house:
            iconImageView.image = UIImage(named: "ic_category_house")
        case .shopping:
            iconImageView.image = UIImage(named: "ic_category_shopping")
        }
        
        let image = iconImageView.image?.withRenderingMode(.alwaysTemplate)
        iconImageView.image = image
        iconImageView.tintColor = .textColor
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  TransactionCell.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit


class TransactionCell: UITableViewCell {
    
    let contentStackView = UIStackView()
    let categoryIconView = CategoryIconView(frame: .zero)
    let descriptionView = TransactionDescriptionStackView()
    let amountView = AmountView()
    
    let cellPadding: CGFloat = 12
    var bottomSpacing: CGFloat = 12
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(contentStackView)
        contentStackView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.equalToSuperview().offset(self.cellPadding)
            $0.trailing.equalToSuperview().offset(-self.cellPadding)
            $0.bottom.equalToSuperview().offset(-self.bottomSpacing)
        }
        contentStackView.axis = .horizontal
        contentStackView.spacing = 8
        contentStackView.alignment = .center
        
        contentStackView.addArrangedSubview(categoryIconView)
        contentStackView.addArrangedSubview(descriptionView)
        contentStackView.addArrangedSubview(amountView)
        amountView.setFont(size: 14)
        
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        descriptionView.setItem(description: .empty, time: .empty)
        amountView.setAmount(0)
        categoryIconView.iconImageView.image = nil
    }
    
    func setItem(transaction: Transaction) {
        categoryIconView.setCategoryStyling(category: transaction.category)
        descriptionView.setItem(description: transaction.description,
                                time: transaction.date.getTimeOfDayString())
        amountView.setAmount(transaction.amount)        
        
        if transaction.direction == .incoming {
            amountView.setTextColor(.incomingAmountColor)
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

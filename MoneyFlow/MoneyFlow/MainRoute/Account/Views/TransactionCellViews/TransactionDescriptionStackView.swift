//
//  TransactionDescriptionStackView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class TransactionDescriptionStackView: UIStackView {
    
    let descriptionLabel = StandardLabel(fontsize: 14)
    let timeLabel = StandardLabel(fontsize: 10)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .vertical
        addArrangedSubview(descriptionLabel)
        addArrangedSubview(timeLabel)
        
        timeLabel.textColor = .lightGray
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setItem(description: String, time: String) {
        descriptionLabel.text = description
        timeLabel.text = time
    }
}

//
//  TransactionTableViewSectionHeaderView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class TransactionTableViewSectionHeaderView: UIView {
    
    let label = UILabel()
    let verticalSpacing: CGFloat = 4
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addSubview(label)
        label.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.centerX.equalToSuperview()
            $0.top.equalToSuperview().offset(self.verticalSpacing)
            $0.bottom.equalToSuperview().offset(-self.verticalSpacing)
        }
        
        label.textColor = .textColor
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 12)
        backgroundColor = .grayBackgroundColor
    }
    
    func setTitle(_ title: String) {
        label.text = title
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

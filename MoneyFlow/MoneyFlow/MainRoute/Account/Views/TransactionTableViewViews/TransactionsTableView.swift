//
//  TransactionsTableView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class TransactionTableView: UITableView {
    
    let disposeBag = DisposeBag()
    let cellId = TransactionCell.description()
    var transactionService: TransactionServiceDelegate?
    var datasource: RxTableViewSectionedReloadDataSource<TransactionSection>?
    
    let emptyListView = EmptyListView()
    
    var selectedCell: IndexPath?
    
    weak var accountDelegate: AccountViewControllerDelegate?
    
    init(transactionService: TransactionServiceDelegate?, accountDelegate: AccountViewControllerDelegate) {
        super.init(frame: .zero, style: .plain)
        self.register(TransactionCell.self, forCellReuseIdentifier: cellId)
        self.transactionService = transactionService
        self.accountDelegate = accountDelegate
        addObservables()
        tableFooterView = UIView()
        estimatedRowHeight = 65
        rowHeight = UITableView.automaticDimension
        
        // Set the inset for separators to zero otherwise they will not be
        // full width in landscape mode.
        separatorInset = UIEdgeInsets.zero
        
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func addObservables() {
        
        // Use the sectioned view from rxdatasources to create section titles
        datasource = RxTableViewSectionedReloadDataSource<TransactionSection> (configureCell: { section, _, indexPath, transaction in
            let cell = TransactionCell()
            cell.setItem(transaction: transaction)
            return cell
        })
        
        guard let dataSource = self.datasource else { return }
        dataSource.titleForHeaderInSection = { dataSource, index in
            guard dataSource.sectionModels.indices.contains(index) else { return .empty }
            return dataSource.sectionModels[index].header
        }
        
        transactionService?
            .accountStore
            .transactionSectionStore
            .asObservable()
            .bind(to: self.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        // Set the background to an empty list view if there are no transactions found.
        transactionService?
            .accountStore
            .transactionSectionStore
            .asObservable()
            .subscribe({ result in
                let listIsEmpty = result.element?.isEmpty == true
                self.backgroundView = listIsEmpty ? self.emptyListView : nil
            }).disposed(by: disposeBag)
        
        self.rx.modelSelected(Transaction.self).bind { [weak self] transaction in
            guard let `self` = self else { return }
            self.accountDelegate?.handleTransactionTap(transaction: transaction)
        }.disposed(by: disposeBag)
        
        // Set delegate for headerview in section.
        rx.setDelegate(self).disposed(by: disposeBag)
    }
    
}

extension TransactionTableView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let datasource = self.datasource else { return nil }
        let headerView = TransactionTableViewSectionHeaderView()
        headerView.setTitle(datasource.sectionModels[section].header)
        return headerView
    }
}

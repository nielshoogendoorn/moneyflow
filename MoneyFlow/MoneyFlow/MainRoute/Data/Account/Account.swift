//
//  Account.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

enum DecodingError: Error {
    case invalidAccountBalance
    case invalidTransactionAmount
    case invalidTransactionDate
}

struct Account: Codable {
    let accountNumber: String
    let balance: Double
    let transactions: [Transaction]
    
    enum CodingKeys: String, CodingKey {
        case accountNumber = "account"
        case balance, transactions
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        accountNumber = try container.decode(String.self, forKey: .accountNumber)
        transactions = try container.decode([Transaction].self, forKey: .transactions)
        let balanceString = try container.decode(String.self, forKey: .balance)
        guard
            let balanceDouble = Double(balanceString)
        else { throw DecodingError.invalidAccountBalance }
        balance = balanceDouble
    }
}

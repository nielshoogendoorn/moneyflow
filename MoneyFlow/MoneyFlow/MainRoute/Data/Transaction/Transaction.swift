//
//  Transaction.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

enum TransactionCategory {
    case shopping, dinner, defaultTransaction, house, coffee
}

enum TransactionDirection {
    case incoming, outgoing
}

struct Transaction: Codable {
    let id, description, otherAccountNumber: String
    let amount: Double
    let date: Date
    var category: TransactionCategory = .defaultTransaction
    var direction: TransactionDirection
    var beforeBalance: Double = 0
    var afterBalance: Double = 0

    enum CodingKeys: String, CodingKey {
        case id, description, amount, date
        case otherAccountNumber = "otherAccount"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        description = try container.decode(String.self, forKey: .description)
        otherAccountNumber = try container.decode(String.self, forKey: .otherAccountNumber)
        
        let dateString = try container.decode(String.self, forKey: .date)
        if let date = dateString.formatToDate() {
            self.date = date
        } else { throw DecodingError.invalidTransactionDate }
        
        let amountString = try container.decode(String.self, forKey: .amount)
        guard
            let amountDouble = Double(amountString)
            else { throw DecodingError.invalidTransactionAmount }
        amount = amountDouble
        direction = amount.isNegative() ? .outgoing : .incoming
        category = TransactionHelper.getCategoryFromDescription(description)
    }
    
    init(id: String, description: String, otherAccountNumber: String, amount: Double, date: Date, category: TransactionCategory, direction: TransactionDirection, beforeBalance: Double, afterBalance: Double) {
        self.id = id
        self.description = description
        self.otherAccountNumber = otherAccountNumber
        self.amount = amount
        self.date = date
        self.category = category
        self.direction = direction
        self.beforeBalance = beforeBalance
        self.afterBalance = afterBalance
    }
    
    func createTransactionWithBeforeBalance(_ beforeBalance: Double) -> Transaction {
        var tempTransaction = self
        tempTransaction.beforeBalance = beforeBalance
        tempTransaction.afterBalance = beforeBalance + self.amount
        return tempTransaction
    }
}

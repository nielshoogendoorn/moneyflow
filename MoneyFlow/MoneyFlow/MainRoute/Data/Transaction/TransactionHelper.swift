//
//  TransactionHelper.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

class TransactionHelper {
    static func getCategoryFromDescription(_ description: String) -> TransactionCategory {
        // Normally we would receive the category from the API.
        let categoryKeywords: [TransactionCategory: [String]] = [
            TransactionCategory.coffee: ["starbucks", "coffee company"],
            TransactionCategory.house: ["rent", "mortgage"],
            TransactionCategory.dinner: ["pizza"],
            TransactionCategory.shopping: ["albert heijn", "dirk", "jumbo"],
        ]
        
        let categoryKeyword = categoryKeywords.first { (category, keywords) -> Bool in
            return keywords.first(where: { description.contains($0) }) != nil
        }
        
        return categoryKeyword?.key ?? TransactionCategory.defaultTransaction
    }
}

extension Sequence where Iterator.Element == Transaction {
    func sortByDateDescending() -> [Transaction] {
        return self.sorted(by: { $0.date > $1.date })
    }
    
    func calculateBeforeAndAfterBalances(currentBalance: Double) -> [Transaction] {
        var tempTransactions: [Transaction] = []
        var tempBalance = currentBalance
        
        self.sortByDateDescending().forEach { transaction in
            let beforeBalance = tempBalance - transaction.amount
            tempBalance = beforeBalance
            tempTransactions
                .append(transaction.createTransactionWithBeforeBalance(beforeBalance))
            
        }
        return tempTransactions
    }
    
    func createTransactionSections(currentBalance: Double) -> [TransactionSection] {
        // First sort transactions by date, descending.
        let sortedTransactions = self.sortByDateDescending()
        
        // Calculate and add the before and after amounts of the transactions.
        let balanceCalculatedTransactions = sortedTransactions.calculateBeforeAndAfterBalances(currentBalance: currentBalance)
        
        // Group all transactions by the day in the year. For example 1st of August 2018.
        let groupedTransactions = Dictionary(grouping: balanceCalculatedTransactions,
                                             by: { $0.date.getDayInYear() })
        
        // Sort the grouped transactions from newest to oldest.
        let sortedGroups = groupedTransactions.sorted(by: { $0.key > $1.key })
        
        // Map the sorted transactions to transaction sections.
        let sections: [TransactionSection] = sortedGroups.compactMap {
            let header = $0.key.getDayInYearString(dateStyle: .long)
            return TransactionSection(header: header, items: $0.value)
        }
        
        return sections
    }
}

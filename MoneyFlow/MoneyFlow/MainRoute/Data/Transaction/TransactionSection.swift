//
//  TransactionSection.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 21/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation
import RxDataSources

struct TransactionSection {
    var header: String
    var items: [TransactionSection.Item]
}

extension TransactionSection: SectionModelType {
    typealias Item = Transaction
    
    init(original: TransactionSection, items: [Transaction]) {
        self = original
        self.items = items
    }
}


//
//  AccountStore.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

protocol AccountStoreDelegate {
    var accountDetailsStore: BehaviorRelay<[Account]> { get }
    var currentBalance: BehaviorRelay<Double> { get }
    var transactionSectionStore: BehaviorRelay<[TransactionSection]> { get }
    
    func updateAccountStore(account: Account)
}

class AccountStore: AccountStoreDelegate {
    var transactionSectionStore: BehaviorRelay<[TransactionSection]> = BehaviorRelay<[TransactionSection]>(value: [])
    var currentBalance: BehaviorRelay<Double> = BehaviorRelay<Double>(value: 0)
    var accountDetailsStore: BehaviorRelay<[Account]> = BehaviorRelay<[Account]>(value: [])
    var transactionStore: BehaviorRelay<[Transaction]> = BehaviorRelay<[Transaction]>(value: [])
    
    func updateAccountStore(account: Account) {
        accountDetailsStore.accept([account])
        let sections = account.transactions.createTransactionSections(currentBalance: account.balance)
        transactionSectionStore.accept(sections)
        transactionStore.accept(account.transactions)
        currentBalance.accept(account.balance)
    }
}

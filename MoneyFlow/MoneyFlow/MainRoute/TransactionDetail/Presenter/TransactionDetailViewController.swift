//
//  TransactionDetailViewController.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class TransactionDetailViewController: UIViewController {
    
    var transaction: Transaction
    let scrollView = UIScrollView()
    let headerView = UIView()
    
    lazy var currentAccountView: CurrentAccountView = {
        return CurrentAccountView(transactionService: self.transactionService)
    }()
    
    let transactionDetailsView = TransactionDetailsContainerView()
    
    var transactionService: TransactionServiceDelegate?
    
    init(transaction: Transaction) {
        self.transaction = transaction
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setDependencies()
        buildInterface()
        title = .transaction
    }
    
    func buildInterface() {
        view.backgroundColor = .grayBackgroundColor
        buildHeaderView()
        buildScrollView()
        buildCurrentAccountView()
        buildTransactionDetailsViewContainer()
    }
    
    fileprivate func buildScrollView() {
        view.addSubview(scrollView)
        scrollView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.top.equalTo(self.headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
        scrollView.showsVerticalScrollIndicator = false
    }
    
    fileprivate func buildHeaderView() {
        guard let navBarFrame = navigationController?.navigationBar.frame else { return }
        let height = navBarFrame.origin.y + navBarFrame.height
        view.addSubview(headerView)
        headerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalTo(height)
        }
        headerView.backgroundColor = .brandPrimary
    }
    
    fileprivate func buildTransactionDetailsViewContainer() {
        scrollView.addSubview(transactionDetailsView)
        transactionDetailsView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.top.equalTo(self.currentAccountView.snp.bottom).offset(8)
            $0.leading.trailing.width.bottom.equalToSuperview()
        }
        transactionDetailsView.setItem(transaction: transaction)
    }
    
    fileprivate func buildCurrentAccountView() {
        scrollView.addSubview(currentAccountView)
        currentAccountView.snp.makeConstraints {
            $0.top.leading.trailing.width.equalToSuperview()
        }
    }
    
    fileprivate func setDependencies() {
        self.transactionService = container?.resolve(TransactionServiceDelegate.self)
    }
}

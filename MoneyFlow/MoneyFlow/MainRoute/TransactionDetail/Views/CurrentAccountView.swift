//
//  CurrentAccountView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CurrentAccountView: UIView {
    let container = UIView()
    let cardView = CardView()
    let accountDetailsStackView = UIStackView()
    let accountNameLabel = StandardBoldLabel(numberOfLines: 2)
    let accountNumberLabel = StandardLabel(fontsize: 12)
    let currentBalanceAmountView = AmountView()
    
    let containerPadding: CGFloat = 8
    let cardPadding: CGFloat = 16
    
    var transactionService: TransactionServiceDelegate?
    let disposeBag = DisposeBag()
    
    init(transactionService: TransactionServiceDelegate?) {
        super.init(frame: .zero)
        self.transactionService = transactionService
        buildInterface()
        addObservers()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func buildInterface() {
        buildContainer()
        buildCardView()
        buildAccountDetailsStackview()
        buildCurrentBalanceAmountView()
    }
    
    fileprivate func buildContainer() {
        addSubview(container)
        container.snp.makeConstraints {
            $0.leading.trailing.bottom.top.equalToSuperview()
        }
        container.backgroundColor = .clear
    }
    
    fileprivate func buildCardView() {
        container.addSubview(cardView)
        cardView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.equalToSuperview().offset(self.containerPadding)
            $0.trailing.bottom.equalToSuperview().offset(-self.containerPadding)
        }
    }
    
    fileprivate func buildAccountDetailsStackview() {
        cardView.addSubviews(views: accountDetailsStackView, currentBalanceAmountView)
        accountDetailsStackView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.equalToSuperview().offset(self.cardPadding)
            $0.bottom.equalToSuperview().offset(-self.cardPadding)
        }
        accountDetailsStackView.axis = .vertical
        accountDetailsStackView.spacing = 4
        
        accountDetailsStackView.addArrangedSubview(accountNameLabel)
        accountNameLabel.text = "\(String.moneyFlow) \(String.checkingsAccount)"
        
        accountDetailsStackView.addArrangedSubview(accountNumberLabel)
    }
    
    fileprivate func buildCurrentBalanceAmountView() {
        currentBalanceAmountView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.centerY.equalToSuperview()
            $0.trailing.equalToSuperview().offset(-self.cardPadding)
            $0.leading.greaterThanOrEqualTo(self.accountDetailsStackView.snp.trailing).offset(8)
        }
    }
    
    func addObservers() {
        transactionService?
            .accountStore
            .accountDetailsStore
            .asObservable()
            .subscribe({ event in
                guard let account = event.element?.first else { return }
                self.setCurrentBalance(amount: account.balance)
                self.setAccountNumber(account.accountNumber)
            }).disposed(by: disposeBag)
    }
    
    func setAccountNumber(_ accountNumber: String) {
        accountNumberLabel.text = accountNumber
    }
    
    func setCurrentBalance(amount: Double) {
        currentBalanceAmountView.setAmount(amount)
    }
}

//
//  TransactionDetailItemView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class TransactionDetailItem: UIStackView {
    let itemName = StandardBoldLabel(fontsize: 14)
    let itemDescription = StandardLabel(fontsize: 14)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        axis = .vertical
        addArrangedSubview(itemName)
        addArrangedSubview(itemDescription)
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setItem(name: String, description: String) {
        itemName.text = name
        itemDescription.text = description
    }
}

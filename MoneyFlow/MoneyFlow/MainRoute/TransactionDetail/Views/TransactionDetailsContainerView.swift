//
//  TransactionDetailsContainerView.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 22/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import UIKit

class TransactionDetailsContainerView: UIView {
    let contentStackView = UIStackView()
    let transactionAmountView = AmountView()
    let otherAccountItem = TransactionDetailItem()
    let descriptionItem = TransactionDetailItem()
    let dateItem = TransactionDetailItem()
    let balanceBeforeItem = TransactionDetailItem()
    let balanceAfterItem = TransactionDetailItem()
    
    let contentSpacing: CGFloat = 16
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        
        buildContentStackView()
        
    }
    
    func setItem(transaction: Transaction) {
        transactionAmountView.setAmount(transaction.amount)
        if transaction.direction == .incoming {
            transactionAmountView.setTextColor(.incomingAmountColor)
        }
        
        otherAccountItem.setItem(name: .account,
                                 description: transaction.otherAccountNumber)
        descriptionItem.setItem(name: .description,
                                description: transaction.description)
        descriptionItem.itemDescription.numberOfLines = 0
        
        let dateString = transaction.date.getDayInYearString(dateStyle: .long)
        dateItem.setItem(name: .date, description: dateString)
        
        let beforeAmountString = transaction.beforeBalance.formatAsCurrencyAmount()
        balanceBeforeItem.setItem(name: .balanceBefore, description: beforeAmountString)
        
        let afterAmountString = transaction.afterBalance.formatAsCurrencyAmount()
        balanceAfterItem.setItem(name: .balanceAfter, description: afterAmountString)
    }
    
    fileprivate func buildContentStackView() {
        addSubview(contentStackView)
        contentStackView.snp.makeConstraints { [weak self] in
            guard let `self` = self else { return }
            $0.leading.top.equalToSuperview().offset(self.contentSpacing)
            $0.trailing.bottom.equalToSuperview().offset(-self.contentSpacing)
        }
        contentStackView.axis = .vertical
        contentStackView.spacing = contentSpacing
        contentStackView.addArrangedSubview(transactionAmountView)
        contentStackView.addArrangedSubview(otherAccountItem)
        contentStackView.addArrangedSubview(descriptionItem)
        contentStackView.addArrangedSubview(dateItem)
        contentStackView.addArrangedSubview(balanceBeforeItem)
        contentStackView.addArrangedSubview(balanceAfterItem)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

//
//  LocalApiManager.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

protocol LocalApiManagerDelegate {
    func getAccountDetails() -> Account?
}

class LocalApiManager: LocalApiManagerDelegate {
    func getAccountDetails() -> Account? {
        
        let filePath = Bundle.main.path(forResource: "transactions", ofType: "json")
        guard let path = filePath else { return nil }
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path))
            let decoder = JSONDecoder()
            let result = try decoder.decode(Account.self, from: data)
            return result
        } catch {
            // Handle error
            return nil
        }
        
    }
}

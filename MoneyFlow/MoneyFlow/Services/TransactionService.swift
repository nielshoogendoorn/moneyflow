//
//  TransactionService.swift
//  MoneyFlow
//
//  Created by Niels Hoogendoorn on 20/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation

protocol TransactionServiceDelegate {
    var accountStore: AccountStoreDelegate { get }
    func loadAccountData()
}

class TransactionService: TransactionServiceDelegate {
    
    var accountStore: AccountStoreDelegate = AccountStore()
    let localApi: LocalApiManagerDelegate = LocalApiManager()
    
    func loadAccountData() {
        if let account = localApi.getAccountDetails() {
            accountStore.updateAccountStore(account: account)
        } else {
            // Handle error
        }
    }
}

//
//  AppDelegateDependenciesTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 24/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class AppDelegateDependenciesTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_appDelegate_transactionServiceIsInjected() {
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let container = appDelegate?.container
        let transactionService = container?.resolve(TransactionServiceDelegate.self)
        XCTAssertNotNil(transactionService)
    }

}

//
//  TransactionDetailsContainerViewTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class AmountViewTests: XCTestCase {
    var sut: AmountView!
    
    override func setUp() {
        super.setUp()
        sut = AmountView()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_setAmount_amountIsOneFifty_amountViewShowsOneEuroAndFiftyCents() {
        sut.setAmount(1.50)
        let euroLabelText = sut.euroLabel.text
        let centLabelText = sut.centsLabel.text
        XCTAssertEqual(euroLabelText, "€ 1,")
        XCTAssertEqual(centLabelText, "50")
    }
    
    func test_setAmount_amountIsMinusOneFifty_amountViewShowsOneEuroAndFiftyCents() {
        sut.setAmount(-1.50)
        let euroLabelText = sut.euroLabel.text
        let centLabelText = sut.centsLabel.text
        XCTAssertEqual(euroLabelText, "€ -1,")
        XCTAssertEqual(centLabelText, "50")
    }
    
    func test_initItem_textColorIsBlackWithAlpha() {
        XCTAssertEqual(sut.euroLabel.textColor, UIColor.textColor)
        XCTAssertEqual(sut.centsLabel.textColor, UIColor.textColor)
    }
    
    func test_contentHugging_centsLabelContentHuggingIsHigherThanDefaultPriority() {
        let defaultPriority: Float = 250
        let centLabelContentHugging = sut.centsLabel.contentHuggingPriority(for: .horizontal).rawValue
        XCTAssert(centLabelContentHugging > defaultPriority)
    }
    
    func test_contentHugging_euroLabelContentHuggingIsHigherThanCentLabel() {
        let euroLabelContentHugging = sut.euroLabel.contentHuggingPriority(for: .horizontal)
        let centLabelContentHugging = sut.centsLabel.contentHuggingPriority(for: .horizontal)
        XCTAssert(euroLabelContentHugging > centLabelContentHugging)
    }
    
    func test_fontSize_centLabelFontsizeIsSixtyPercentOfEuroLabelFontSize() {
        let centLabelFontsize = sut.centsLabel.font.pointSize
        let euroLabelFontsize = sut.euroLabel.font.pointSize
        XCTAssertEqual(centLabelFontsize, euroLabelFontsize * 0.6)
    }
}

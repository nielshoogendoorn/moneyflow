//
//  TransactionHelperTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class TransactionHelperTests: XCTestCase {
    var sut: TransactionHelper!
    
    override func setUp() {
        super.setUp()
        sut = TransactionHelper()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }
    
    func test_GetCategoryFromDescription_descriptionIsStarbucks_returnCoffee() {
        let description = "starbucks"
        let category = TransactionHelper.getCategoryFromDescription(description)
        XCTAssertEqual(category, TransactionCategory.coffee)
    }
    
    func test_GetCategoryFromDescription_descriptionIsRent_returnHouse() {
        let description = "rent"
        let category = TransactionHelper.getCategoryFromDescription(description)
        XCTAssertEqual(category, TransactionCategory.house)
    }
    
    func test_GetCategoryFromDescription_descriptionIsPizza_returnDinner() {
        let description = "pizza"
        let category = TransactionHelper.getCategoryFromDescription(description)
        XCTAssertEqual(category, TransactionCategory.dinner)
    }
    
    func test_GetCategoryFromDescription_descriptionIsDirk_returnShopping() {
        let description = "dirk"
        let category = TransactionHelper.getCategoryFromDescription(description)
        XCTAssertEqual(category, TransactionCategory.shopping)
    }
    
    func test_GetCategoryFromDescription_descriptionIsEmpty_returnDefault() {
        let description = ""
        let category = TransactionHelper.getCategoryFromDescription(description)
        XCTAssertEqual(category, TransactionCategory.defaultTransaction)
    }
    
    func test_transactionList_sortByDateDescending() {
        let today = Date()
        guard let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        else {
            XCTFail("Incorrect date")
            return
        }
        
        let currentList: [Transaction] = [
            TestUtils.createTransaction(id: "1", date: yesterday),
            TestUtils.createTransaction(id: "2", date: today)
        ]
        let newList = currentList.sortByDateDescending()
        XCTAssertEqual(newList[0].id, currentList[1].id)
        XCTAssertEqual(newList[1].id, currentList[0].id)
    }
    
    func test_calculateBeforeAndAfterBalances_currentBalanceIsTenAmountIsOne_beforeAndAterBalancesAreNotZero() {
        let currentList: [Transaction] = [
            TestUtils.createTransaction(amount: 1.00)
        ]
        let newList: [Transaction] = currentList.calculateBeforeAndAfterBalances(currentBalance: 10.00)
        let zero: Double = 0.00
        newList.forEach {
            XCTAssertNotEqual($0.beforeBalance, zero)
            XCTAssertNotEqual($0.afterBalance, zero)
        }
    }
    
    func test_calculateBeforeAndAfterBalances_currentBalanceIsTenTransactionIsOne_beforeAndAterBalancesAreNotZero() {
        let currentBalance: Double = 10.00
        let amount: Double = 1
        let correctBeforeFirst: Double = 9.00
        let correctAfterFirst: Double = 10.00
        let correctBeforeSecond: Double = 8.00
        let correctAfterSecond: Double = 9.00
        
        let currentList: [Transaction] = [
            TestUtils.createTransaction(amount: amount),
            TestUtils.createTransaction(amount: amount)
        ]
        let newList: [Transaction] = currentList.calculateBeforeAndAfterBalances(currentBalance: currentBalance)
        
        XCTAssertEqual(newList[0].beforeBalance, correctBeforeFirst)
        XCTAssertEqual(newList[0].afterBalance, correctAfterFirst)
        XCTAssertEqual(newList[1].beforeBalance, correctBeforeSecond)
        XCTAssertEqual(newList[1].afterBalance, correctAfterSecond)
    }
    
    func test_createTransactionSections_twoDatesTransactions_returnTwoSections() {
        let today = Date()
        guard let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())
            else {
                XCTFail("Incorrect date")
                return
        }
        
        let currentList: [Transaction] = [
            TestUtils.createTransaction(id: "1", date: yesterday),
            TestUtils.createTransaction(id: "2", date: today)
        ]
        let sections = currentList.createTransactionSections(currentBalance: 10.00)
        XCTAssertEqual(sections.count, 2)
    }
    
    func test_createTransactionSections_dateIsToday_returnCorrectSectionHeader() {
        let today = Date()
        
        let currentList: [Transaction] = [
            TestUtils.createTransaction(id: "1", date: today)
        ]
        let sections = currentList.createTransactionSections(currentBalance: 10.00)
        XCTAssertEqual(sections.first?.header, today.getDayInYearString(dateStyle: .long))
    }
}

//
//  TransactionTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class TransactionTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_createTransactionWithBeforeBalance_beforeBalanceIsOneAmountIsTwo() {
        let amount: Double = 2.00
        let before: Double = 1.00
        let newAfter: Double = amount + before
        let currentTransaction = TestUtils.createTransaction(amount: amount)
        let newTransaction = currentTransaction.createTransactionWithBeforeBalance(before)
        
        XCTAssertEqual(newTransaction.afterBalance, newAfter)
        XCTAssertEqual(newTransaction.beforeBalance, before)
    }

    func test_createTransactionWithBeforeBalance_beforeBalanceIsMinusOneAmountIsTwo() {
        let amount: Double = 2.00
        let before: Double = -1.00
        let newAfter: Double = amount + before
        let currentTransaction = TestUtils.createTransaction(amount: amount)
        let newTransaction = currentTransaction.createTransactionWithBeforeBalance(before)
        
        XCTAssertEqual(newTransaction.afterBalance, newAfter)
        XCTAssertEqual(newTransaction.beforeBalance, before)
    }
}

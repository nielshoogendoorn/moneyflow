//
//  DateTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 24/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class DateTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_getTimeOfDayString_validDate_timeIsNotNil() {
        let dateString = "2018-05-14T08:19:00Z"
        let date = dateString.formatToDate()
        let time = date?.getTimeOfDayString()
        XCTAssertNotNil(time)
    }

    func test_getTimeOfDayString_validDate_returnsCorrectHourAndMinute() {
        let dateString = "2018-05-14T08:19:00Z"
        let date = dateString.formatToDate()
        let time = date?.getTimeOfDayString()
        let correctTimeAdjustedForTimeZone = "10:19"
        XCTAssertEqual(time, correctTimeAdjustedForTimeZone)
    }
    
    func test_getTimeOfDayString_invalidDate_timeIsNil() {
        let dateString = "invalid"
        let date = dateString.formatToDate()
        let time = date?.getTimeOfDayString()
        XCTAssertNil(time)
    }
    
    func test_getTimeOfDayString_invalidTime_timeIsNil() {
        let dateString = "2018-05-14Tinvalid"
        let date = dateString.formatToDate()
        let time = date?.getTimeOfDayString()
        XCTAssertNil(time)
    }
    
    func test_getTimeOfDayString_noTime_timeIsNil() {
        let dateString = "2018-05-14"
        let date = dateString.formatToDate()
        let time = date?.getTimeOfDayString()
        XCTAssertNil(time)
    }
}

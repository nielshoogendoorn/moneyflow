//
//  DoubleTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 24/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class DoubleTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_formatAsCurrencyAmount_amountIsOne_returnPositiveAmount() {
        let amount: Double = 1.00
        let amountString = amount.formatAsCurrencyAmount()
        XCTAssertEqual(amountString, "€ 1,00")
    }
    
    func test_formatAsCurrencyAmount_amountIsZero_returnPositiveAmount() {
        let amount: Double = 0.00
        let amountString = amount.formatAsCurrencyAmount()
        XCTAssertEqual(amountString, "€ 0,00")
    }
    
    func test_formatAsCurrencyAmount_amountIsMinusOne_returnNegativeAmount() {
        let amount: Double = -1.00
        let amountString = amount.formatAsCurrencyAmount()
        XCTAssertEqual(amountString, "€ -1,00")
    }
    
    func test_isNegative_amountIsOne_returnFalse() {
        let amount: Double = 1.00
        XCTAssertFalse(amount.isNegative())
    }
    
    func test_isNegative_amountIsZero_returnFalse() {
        let amount: Double = 0.00
        XCTAssertFalse(amount.isNegative())
    }
    
    func test_isNegative_amountIsMinusOne_returnTrue() {
        let amount: Double = -1.00
        XCTAssertTrue(amount.isNegative())
    }
    
}

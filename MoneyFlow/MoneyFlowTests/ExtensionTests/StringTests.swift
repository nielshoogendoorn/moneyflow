//
//  StringTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 24/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class StringTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_formatToDate_inputIsValidDate_returnDate() {
        let dateString = "2018-05-14T08:19:00Z"
        let date = dateString.formatToDate()
        XCTAssertNotNil(date)
    }
    
    func test_formatToDate_inputIsInvalidDate_returnNil() {
        let dateString = "invalid"
        let date = dateString.formatToDate()
        XCTAssertNil(date)
    }
}

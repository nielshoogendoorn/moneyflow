//
//  ApiManagerTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class ApiManagerTests: XCTestCase {

    var sut: LocalApiManagerDelegate!
    
    override func setUp() {
        sut = LocalApiManager()
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_getAccountDetails_returnsValidAccount() {
        let account = sut.getAccountDetails()
        XCTAssertNotNil(account)
    }

}

//
//  AccountStoreTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class AccountStoreTests: XCTestCase {
    
    var sut: AccountStore!
    var apiManager: LocalApiManagerDelegate!
    
    override func setUp() {
        super.setUp()
        sut = AccountStore()
        apiManager = LocalApiManager()
    }

    override func tearDown() {
        sut = nil
        apiManager = nil
        super.tearDown()
    }

    func test_updateAccountStore_accountDetailStoreHasJsonAccount() {
        guard let jsonAccount = apiManager.getAccountDetails() else {
            XCTFail("Could not get local json file")
            return
        }
        
        sut.updateAccountStore(account: jsonAccount)
        XCTAssertEqual(sut.accountDetailsStore.value.first?.accountNumber,
                       jsonAccount.accountNumber)
    }
    
    func test_transactionSectionStore_sectionsStoreHasJsonAccountSectionNumber() {
        guard let jsonAccount = apiManager.getAccountDetails() else {
            XCTFail("Could not get local json file")
            return
        }
        let correctNumberOfSections = jsonAccount.transactions.createTransactionSections(currentBalance: 10).count
        sut.updateAccountStore(account: jsonAccount)
        XCTAssertEqual(sut.transactionSectionStore.value.count,
                       correctNumberOfSections)
    }
    
    func test_transactionStore_transactionStoreHasJsonAccountTransactionNumber() {
        guard let jsonAccount = apiManager.getAccountDetails() else {
            XCTFail("Could not get local json file")
            return
        }
        let correctNumberOfTransaction = jsonAccount.transactions.count
        sut.updateAccountStore(account: jsonAccount)
        XCTAssertEqual(sut.transactionStore.value.count,
                       correctNumberOfTransaction)
    }
    
    func test_currentBalance_transactionStoreHasJsonAccountCurrentBalance() {
        guard let jsonAccount = apiManager.getAccountDetails() else {
            XCTFail("Could not get local json file")
            return
        }
        let correctCurrentBalance = jsonAccount.balance
        sut.updateAccountStore(account: jsonAccount)
        XCTAssertEqual(sut.currentBalance.value, correctCurrentBalance)
    }
}

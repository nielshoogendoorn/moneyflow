//
//  File.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import Foundation
@testable import MoneyFlow

class TestUtils {
    static func createTransaction(id: String = .empty, description: String = .empty, otherAccountNumber: String = .empty, amount: Double = 0.00, date: Date = Date(), category: TransactionCategory = .defaultTransaction, direction: TransactionDirection = .incoming, beforeBalance: Double = 0.00, afterBalance: Double = 0.00) -> Transaction {
        return Transaction(id: id, description: description, otherAccountNumber: otherAccountNumber, amount: amount, date: date, category: category, direction: direction, beforeBalance: beforeBalance, afterBalance: afterBalance)
    }
}

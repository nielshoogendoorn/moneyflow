//
//  TransactionDetailsContainerViewTests.swift
//  MoneyFlowTests
//
//  Created by Niels Hoogendoorn on 23/07/2019.
//  Copyright © 2019 Niels Hoogendoorn. All rights reserved.
//

import XCTest
@testable import MoneyFlow

class TransactionDetailsContainerViewTests: XCTestCase {
    var sut: TransactionDetailsContainerView!
    
    override func setUp() {
        super.setUp()
        sut = TransactionDetailsContainerView()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func test_setItem_amountIsOneFifty_amountViewShowsOneFifty() {
        let transaction = TestUtils.createTransaction(amount: 1.50)
        sut.setItem(transaction: transaction)
        let euroLabelText = sut.transactionAmountView.euroLabel.text
        let centLabelText = sut.transactionAmountView.centsLabel.text
        XCTAssertEqual(euroLabelText, "€ 1,")
        XCTAssertEqual(centLabelText, "50")
    }

    func test_setItem_directionIsIncoming_amountViewColorIsIncomingAmountColor() {
        let transaction = TestUtils.createTransaction(direction: .incoming)
        sut.setItem(transaction: transaction)
        let euroLabelText = sut.transactionAmountView.euroLabel.textColor
        let centLabelText = sut.transactionAmountView.centsLabel.textColor
        XCTAssertEqual(euroLabelText, UIColor.incomingAmountColor)
        XCTAssertEqual(centLabelText, UIColor.incomingAmountColor)
    }
    
    func test_setItem_directionIsOutgoing_amountViewColorIsIncomingAmountColor() {
        let transaction = TestUtils.createTransaction(direction: .outgoing)
        sut.setItem(transaction: transaction)
        let euroLabelText = sut.transactionAmountView.euroLabel.textColor
        let centLabelText = sut.transactionAmountView.centsLabel.textColor
        XCTAssertEqual(euroLabelText, UIColor.textColor)
        XCTAssertEqual(centLabelText, UIColor.textColor)
    }
    
    func test_setItem_otherAccountItemIsSetCorrectly() {
        let otherAccountNumber = "correctAccount"
        let transaction = TestUtils.createTransaction(otherAccountNumber: otherAccountNumber)
        sut.setItem(transaction: transaction)
        let otherAccountItem = sut.otherAccountItem
        XCTAssertEqual(otherAccountItem.itemName.text, String.account)
        XCTAssertEqual(otherAccountItem.itemDescription.text, otherAccountNumber)
    }
    
    func test_setItem_descriptionItemIsSetCorrectly() {
        let description = "correctDescription"
        let transaction = TestUtils.createTransaction(description: description)
        sut.setItem(transaction: transaction)
        let descriptionItem = sut.descriptionItem
        XCTAssertEqual(descriptionItem.itemName.text, String.description)
        XCTAssertEqual(descriptionItem.itemDescription.text, description)
        XCTAssertEqual(descriptionItem.itemDescription.numberOfLines, 0)
    }
    
    func test_setItem_dateItemIsSetCorrectly() {
        let dateToTest = Date()
        let correctDate = dateToTest.getDayInYearString(dateStyle: .long)
        let transaction = TestUtils.createTransaction(date: dateToTest)
        sut.setItem(transaction: transaction)
        let dateItem = sut.dateItem
        XCTAssertEqual(dateItem.itemName.text, String.date)
        XCTAssertEqual(dateItem.itemDescription.text, correctDate)
    }
    
    func test_setItem_beforeAmountItemIsSetCorrectly() {
        let correctAmount: Double = 1.00
        let correctAmountString = correctAmount.formatAsCurrencyAmount()
        let transaction = TestUtils.createTransaction(beforeBalance: correctAmount)
        sut.setItem(transaction: transaction)
        let balanceItem = sut.balanceBeforeItem
        XCTAssertEqual(balanceItem.itemName.text, String.balanceBefore)
        XCTAssertEqual(balanceItem.itemDescription.text, correctAmountString)
    }
    
    func test_setItem_afterAmountItemIsSetCorrectly() {
        let correctAmount: Double = 1.00
        let correctAmountString = correctAmount.formatAsCurrencyAmount()
        let transaction = TestUtils.createTransaction(afterBalance: correctAmount)
        sut.setItem(transaction: transaction)
        let balanceItem = sut.balanceAfterItem
        XCTAssertEqual(balanceItem.itemName.text, String.balanceAfter)
        XCTAssertEqual(balanceItem.itemDescription.text, correctAmountString)
    }
    
    
}
